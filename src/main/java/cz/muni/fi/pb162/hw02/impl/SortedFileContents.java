package cz.muni.fi.pb162.hw02.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Implements basic functionality over a file abstraction.
 *
 * @author Ales Horna
 */
class SortedFileContents {
    private final TreeMap<String, Integer> lines = new TreeMap<>();
    private int numOfUniques = 0;
    private int numOfDuplicates = 0;
    private Cardinality cardinality;

    /**
     * Creates new instance of SortedFileContents.
     *
     * @param file List of strings (lines) of a file.
     * @param cardinality Specifies which lines should be omitted.
     */
    SortedFileContents(List<String> file, Cardinality cardinality) {
        this.cardinality = cardinality;

        for (String line: file) {
            if (!lines.containsKey(line)) {
                numOfUniques += 1;
                lines.put(line, 1);
            } else {
                if (lines.get(line) == 1) {
                    numOfDuplicates++;
                }
                lines.replace(line, lines.get(line) + 1);
            }
        }
    }

    /**
     * Prints unique lines from a file. Lines are sorted.
     */
    public void printUniqueLines() {
        for (Map.Entry<String, Integer> entry: lines.entrySet()) {
            if (entry.getValue() == 1) {
                System.out.println(entry.getKey());
            }
        }
    }

    /**
     * Prints duplicate lines from a file. Lines are sorted.
     */
    public void printDuplicateLines() {
        for (Map.Entry<String, Integer> entry: lines.entrySet()) {
           if (entry.getValue() != 1) {
               System.out.println(entry.getKey());
           }
        }
    }

    /**
     * Prints sorted file. No lines are omitted.
     */
    public void printAllLines() {
        for (Map.Entry<String, Integer> entry: lines.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                System.out.println(entry.getKey());
            }
        }
    }

    /**
     * Prints sorted file. Some lines may be omitted due to cardinality setting.
     */
    public void printLines() {
        switch(cardinality) {
            case FULL:
                printAllLines();
                break;
            case DUPLICATE:
                printDuplicateLines();
                break;
            case UNIQUE:
                printUniqueLines();
                break;
            default:
                break;
        }
    }

    /**
     * Prints all lines based on cardinality and it's corresponding sizes.
     */
    public void printSizes() {
        for (Map.Entry<String, Integer> entry: lines.entrySet()) {
            switch (cardinality) {
                case DUPLICATE:
                    if (entry.getValue() != 1) {
                        System.out.println(entry.getKey().length() + ": " + entry.getKey());
                    }
                    break;
                case UNIQUE:
                    if (entry.getValue() == 1) {
                        System.out.println(entry.getKey().length() + ": " + entry.getKey());
                    }
                    break;
                case FULL:
                    System.out.println(entry.getKey().length() + ": " + entry.getKey());
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Prints number of lines in a file. Some lines may be ignored based on cardinality.
     *
     */
    public void countLines() {
        int count = 0;
        switch (cardinality) {
            case DUPLICATE:
                count = numOfDuplicates;
                break;
            case UNIQUE:
                count = numOfUniques;
                break;
            case FULL:
                count = lines.size();
                break;
            default:
                break;
        }

        System.out.println(count);
    }


    /**
     * Prints pair of most similar lines in a file and their distance. In case there is more pairs
     * with same distance prints all of them. Some lines may be omitted based on cardinality.
     */
    public void printMostSimilar() {
        int distance = Integer.MAX_VALUE;

        HashMap<String, String> results = new HashMap<>();

        for (Map.Entry<String, Integer> entry: lines.entrySet()) {
            for (Map.Entry<String, Integer> entry2: lines.entrySet()) {
                if (entry.getKey().equals(entry2.getKey())) {
                    continue;
                }

                int curDistance = LevenshteinDistance.getDistance(entry.getKey(), entry2.getKey());

                if (distance == curDistance) {
                    results.put(entry.getKey(), entry2.getKey());
                } else if(distance > curDistance) {
                    distance = curDistance;
                    results.clear();
                    results.put(entry.getKey(), entry2.getKey());
                }
            }
        }

        System.out.println("Distance of " + distance);
        for (Map.Entry<String, String> entry: results.entrySet()) {
            System.out.println(entry.getKey() + " ~= " + entry.getValue());
        }


    }

    public Cardinality getCardinality() {
        return cardinality;
    }

    public void setCardinality(Cardinality cardinality) {
        this.cardinality = cardinality;
    }

}
