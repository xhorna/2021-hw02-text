package cz.muni.fi.pb162.hw02.impl;

/**
 * Library to count levenshtein distance between two strings.
 */
public class LevenshteinDistance {

    /**
     * Computes Levenshtein's distance of two strings.
     *
     * @param str1 Input string 1
     * @param str2 Input string 2
     * @return Distance between str1 and str2
     */
    public static int getDistance(String str1, String str2) {
        int[] table = new int[str2.length() + 1];

        for (int i = 0; i < str1.length() + 1; i++) {
            int last = i;
            for (int j = 0; j < str2.length() + 1; j++) {
                if (i == 0) {
                    table[j] = j;
                } else {
                    if (j > 0) {
                        int newValue = table[j - 1];
                        if (str1.charAt(i - 1) != str2.charAt(j - 1)) {
                            newValue = Math.min(newValue, Math.min(last, table[j])) + 1;
                        }
                        table[j - 1] = last;
                        last = newValue;
                    }
                }
            }
            if (i > 0) {
                table[table.length - 1] = last;
            }

        }
        return table[table.length - 1];
    }
}
