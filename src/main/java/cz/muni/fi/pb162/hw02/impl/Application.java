package cz.muni.fi.pb162.hw02.impl;

import com.beust.jcommander.Parameter;
import cz.muni.fi.pb162.hw02.FileLoader;
import cz.muni.fi.pb162.hw02.TerminalOperation;
import cz.muni.fi.pb162.hw02.cmd.CommandLine;
import cz.muni.fi.pb162.hw02.cmd.TerminalOperationConverter;

import java.io.IOException;
import java.util.List;


/**
 * Application class represents the command line interface of this application.
 * <p>
 * You are expected to implement  the  {@link Application#run(CommandLine)} method
 *
 * @author jcechace
 */
public class Application {

    @Parameter(names = "--help", help = true)
    private boolean showUsage = false;

    @Parameter(description = "Terminal Operation", converter = TerminalOperationConverter.class)
    private TerminalOperation operation = TerminalOperation.LINES;

    @Parameter(names = {"-u"})
    private boolean filterUniqueLines = false;

    @Parameter(names = {"-s"})
    private boolean sortLines = false;

    @Parameter(names = {"-d"})
    private boolean filterDuplicateLines = false;


    @Parameter(names = {"--file"})
    private String path;

    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     */
    public static void main(String[] args) {
        Application app = new Application();

        CommandLine cli = new CommandLine(app);
        cli.parseArguments(args);

        if (app.showUsage) {
            cli.showUsage();
        } else {
            app.run(cli);
        }
    }

    /**
     * Application runtime logic
     *
     * @param cli command line interface
     */
    private void run(CommandLine cli) {
        List<String> rawFileContents = null;

        if (filterUniqueLines && filterDuplicateLines) {
            System.err.println("Invalid combination of options was used!");
        }

        try {
            FileLoader fileLoader = new FileLoader();
            rawFileContents = fileLoader.loadAsLines(path);

        } catch (IOException exc) {
            System.err.println("Unable to process file '" + path + "'!");
            return;
        }

        Cardinality cardinality = Cardinality.FULL;

        if (filterDuplicateLines) {
            cardinality = Cardinality.DUPLICATE;
        }else if (filterUniqueLines) {
            cardinality = Cardinality.UNIQUE;
        }

        SortedFileContents fileContents = new SortedFileContents(rawFileContents, cardinality);

        switch (operation) {
            case LINES:
                fileContents.printLines();
                break;
            case COUNT:
                fileContents.countLines();
                break;
            case SIZES:
                fileContents.printSizes();
                break;
            case SIMILAR:
                fileContents.printMostSimilar();
                break;
            default:
                break;
        }
    }
}
