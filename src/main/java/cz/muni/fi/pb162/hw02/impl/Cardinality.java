package cz.muni.fi.pb162.hw02.impl;

/**
 * Defines on which lines of a file operations should be executed.
 */
public enum Cardinality {
    FULL, UNIQUE, DUPLICATE
}
